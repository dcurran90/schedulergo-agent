package main

import (
    "bytes"
    "crypto/tls"
    "crypto/x509"
    "encoding/json"
    "flag"
    "fmt"
    "io"
    "io/ioutil"
    "math/rand"
    "net"
    "net/http"
    "os"
    "time"

    "github.com/gorilla/mux"
    "github.com/gorilla/handlers"
)

type WebReturn struct {
    Status int      `json: "status"`
    Message string  `json: "message"`
}

var (
    conf        Mycnf
    installflag bool
    configfile  string

    pool *x509.CertPool
    client *http.Client
)

func chkErr(err error) {
    if err != nil {
        panic(err)
    }
}

func init() {
    flag.BoolVar(&installflag, "install", false, "register with master server")
    flag.StringVar(&configfile, "c", "", "pass the .toml file to read master details from")
    flag.Parse()
    conf = ReadConf()

    pool = x509.NewCertPool()
    pool.AppendCertsFromPEM(pemCerts)
    client = &http.Client{
        Transport: &http.Transport{
            TLSClientConfig: &tls.Config{RootCAs: pool},
        },
        Timeout: 10,
    }

    tr := &http.Transport{
        TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
    }
    client = &http.Client{Transport: tr}
}

func main() {
    // check if we're registering with a new master
    if installflag {
        master := conf.Master.Server
        port := conf.Master.Port
        groups := conf.Master.Groups
        addr := getAddr()
        token := randStr(12)

        AgentRegister(master, addr, token, groups, port)
        // write to a text file containing:
        // master IP, Port and token
    }

    bindserver := conf.General.Bind
    bindport := conf.General.Port
    sslcert := conf.General.SSLCert
    sslkey := conf.General.SSLKey

    r := mux.NewRouter()

    r.HandleFunc("/status", StatusHandler)
    r.HandleFunc("/run", RunHandler).Methods("POST")
    err := http.ListenAndServeTLS(fmt.Sprintf("%s:%d", bindserver, bindport), sslcert, sslkey, handlers.LoggingHandler(os.Stdout, r))
    chkErr(err)
}

func StatusHandler(w http.ResponseWriter, r *http.Request) {
    WebResult(w, http.StatusOK, "API is running")
}

func AgentRegister(master, addr, token, groups string, port int) {
    var ret WebReturn
    conurl := fmt.Sprintf("https://%s:%d/agents/register", master, port)

    data := map[string]string{"address": addr, "token": token, "groups": groups}
    jsonData, err := json.Marshal(data)
    chkErr(err)
    request, err := http.NewRequest("POST", conurl, bytes.NewBuffer(jsonData))
    chkErr(err)
    request.Header.Set("Content-Type", "application/json")

    resp, err := client.Do(request)
    chkErr(err)
    defer resp.Body.Close()
    retdata, err := ioutil.ReadAll(io.LimitReader(resp.Body, 1048576))
    chkErr(err)
    err = json.Unmarshal(retdata, &ret)
    chkErr(err)

    fmt.Println(fmt.Sprintf("%d: %s", ret.Status, ret.Message))

    if ret.Status == http.StatusOK {
        // write details out to file
        f, err := os.OpenFile("/etc/scheduler/masters.txt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
        chkErr(err)
        defer f.Close()
        _, err = f.Write([]byte(master))
        chkErr(err)
    } else {
        // log an error
    }


}

/*
Either take an Ip address from the given configuration file
or
Attempt to guess what IP address to use from the list of available interfaces
it should return the first non loopback interface IP address
*/
func getAddr() string {
    if conf.Master.Address != "" {
        return conf.Master.Address
    } else {
        // Get a list of all interface and loop over, returning one that is not a loopback
        addrs, err := net.InterfaceAddrs()
        chkErr(err)
        for _, address := range addrs {
            if ipnet, ok := address.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
                return ipnet.IP.String()
            }
        }
    }
    return ""
}

/*
Generate a random string of letters and numbers
Returns string
*/
func randStr(n int) string {
    rand.Seed(time.Now().Unix())
    const letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789"
    b := make([]byte, n)
    for i := range b {
        b[i] = letters[rand.Intn(len(letters))]
    }
    return string(b)
}

/*
Generic way to return JSON data
*/
func WebResult(w http.ResponseWriter, status int, msg string) {
    var ret WebReturn
    w.Header().Set("Content-Type", "application/json; charset=UTF-8")
    w.WriteHeader(status)
    ret.Status = status
    ret.Message = msg
    jsonbytes, err := json.Marshal(&ret)
    chkErr(err)
    w.Write(jsonbytes)
}
