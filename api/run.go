package main

import (
    "bytes"
    "encoding/json"
    "io"
    "io/ioutil"
    "net/http"
    "os/exec"
    "strings"
    "syscall"
)

type JobStruct struct {
    Id      int     `json: "id"`
    Name    string  `json: "name"`
    Command string  `json: "command"`
}

type JobReturn struct {
    Id          int     `json: "id"`
    Name        string  `json: "name"`
    ExitStatus  int     `json: "exitstatus"`
    Message     string  `json: "message"`
    Errors      string  `json@ "errors"`
}

var (
    exitstatus int
)

/*
POST
https://myagent.com:8080/run
Run a command for a given job on the agent server
Takes the following parameters:
    * id
    * name
    * command
*/
func RunHandler(w http.ResponseWriter, r *http.Request) {
    var job JobStruct
    body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
    chkErr(err)
    defer r.Body.Close()

    err = json.Unmarshal(body, &job)
    chkErr(err)

    // get the command for the job and split into seperate parts for exec.Command()
    command := strings.Fields(job.Command)
    cmd := exec.Command(command[0], command[1:]...)


    // read and get the stdout and stderr into &out and pass this back to the master
    // along with the exit status of the command.
    var stout bytes.Buffer
    var sterr bytes.Buffer
    cmd.Stdout = &stout
    cmd.Stderr = &sterr
    err = cmd.Start()
    chkErr(err)
    if err = cmd.Wait(); err != nil {
        // gets the exist status, we assume 0 if there is no error
        if exiterr, ok := err.(*exec.ExitError); ok {
            if status, ok := exiterr.Sys().(syscall.WaitStatus); ok {
                exitstatus = status.ExitStatus()
            }
        }
    }
    succ := stout.String()
    fail := sterr.String()
    var jobret JobReturn
    jobret.Name = job.Name
    jobret.Id = job.Id
    jobret.ExitStatus = exitstatus
    jobret.Message = succ
    jobret.Errors = fail

    // finally return data to the master process
    jsonbytes, err :=json.Marshal(&jobret)
    w.Header().Set("Content-Type", "application/json; charset=UTF-8")
    w.WriteHeader(http.StatusOK)
    w.Write(jsonbytes)

}
