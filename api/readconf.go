package main

import (
    "github.com/BurntSushi/toml"
)

type Mycnf struct {
    General general
    Master  master
}

type general struct {
    Bind string
    Port int
    SSLCert string
    SSLKey string
}

type master struct {
    Server string
    Port int
    Groups string
    Address string
}

func ReadConf() Mycnf {
    var config Mycnf
    _, err := toml.DecodeFile(configfile, &config)
    chkErr(err)
    return config
}
