# schedulergo-agent

Agent for schedulergo

# Installation

To install this just download the schedulergo-agent binary from /bin and run it

You can place this in /usr/bin/ for ease of running


# Register an agent with a master

Create a .toml file with the following content:

```toml
[master]
server = "192.168.0.3"
port = 8080
groups = "general"
address = "192.168.0.4"
```

server - IP/hostname of the master server
port - port the master server is listening on
groups - what group(s) to put the agent into, comma separated
address - IP/hostname of the agent. Used for connections and as an identifier. If left blank then registration will guess at the IP address using the first non loopback interface IP

Run the binary with -install option and -c passing the newly created .toml file

```bash
schedulergo-agent -install -c conf.toml
```

This will connect to the master on endpoint */agents/register* and should receive a 200 status with message "Agent Registered"
